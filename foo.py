a = {
    "key2":{
            "general":{
                "age":45,
                "name":"john"
            },
            "additional":{
                "first_commit":{
                    "list_of_attempts":[
                        {
                            "type1":"main",
                            "type":"extra"
                        },
                        {
                            "data":5
                        }
                    ]
                }
            },
            "car_data":{
                "general":{
                    "type1":"mercedes",
                    "model1":"GLE"
                }
            }
        },
        "key1":"google"
}

b = {"key1":"google",
    "key2":{
            "general":{
                "age":45,
                "name":"john"
            },
            "additional":{
                "first_commit":{
                    "list_of_attempts":[ 
                        {
                            "type1":"main",
                            "type":"extra"
                        },
                        
                        {
                            "data":5
                        },
                       
                    ]
                }
            },
            "car_data":{
                "general":{
                    "type1":"mercedes",
                    "model1":"GLE"
                }
            }
        }
}



