from typing import NamedTuple
import pprint
import traceback
from foo import a,b


class keyset(NamedTuple):
    keys_common: set
    keys_only_first: set
    keys_only_second:set

def object_compare(first,second) -> bool:
    '''
    compares two objects and returns True if value is same
    '''
    if first is second:
        return True
    if first == second:
        return True
    else:
        return False

def compare_keys(A:dict,B:dict) -> NamedTuple:
    '''
    Gives common and uncommon keys in a dictionary
    '''
    if not (isinstance(A,dict) and isinstance(B,dict)):
        raise TypeError("compare_keys accepts only dictionaries ")
    A_keys = set(A.keys())
    B_keys = set(B.keys())
    keys_common = A_keys & B_keys
    keys_only_first = A_keys - keys_common
    keys_only_second = B_keys - keys_common
    return keyset(keys_common,keys_only_first,keys_only_second)



def dict_compare(first,second):
    '''
    compares first and second dictionaries and gives a list of differences
    '''
    if not (isinstance(first,dict) and isinstance(first,dict)):
        raise TypeError("   ")
    if object_compare(first,second):
        return []
    list_differences = []
    keyset = compare_keys(first,second)
    for key in keyset.keys_only_first:
        append_string_first  = "key {key!r} is present only  in the first dictionary"
        list_differences.append(append_string_first.format(key = key))
    
    for key in keyset.keys_only_second:
        append_string_second  = "key {key!r} is present only  in the second dictionary"
        list_differences.append(append_string_second.format(key = key))
    
    for key in keyset.keys_common:
        #check if both values are of same type
        if isinstance(first[key],type(second[key])):
            #check if both values are of dict type
            if isinstance(first[key],dict):
                res = dict_compare(first[key],second[key])
                if res != []:
                    key_string = ["For key {k!r} In it's value".format(k = key)]
                else:
                    key_string =[]
                #return key_string + res
                list_differences.append(key_string + res)
            else:
                #Both values are not of dict type
                res = object_compare(first[key],second[key])
                if not res :
                    value_not_same  = "For key {key!r}, values in both dictionaries are different."
                    list_differences.append(value_not_same.format(key = key))
        else:
            # both values are of different type
            diff_type = "For key {key!r} both values are of different type"
            list_differences.append(diff_type.format(key = key))
    return list_differences
            
    
if __name__ == '__main__':
    try:
        pprint.pprint(dict_compare(a, b),indent = 2)
        with open('output.txt','w') as output:
            output.write(pprint.pformat(dict_compare(a, b),indent = 2))
    except TypeError as E:
        print(E)
        print(traceback.format_exc())
    